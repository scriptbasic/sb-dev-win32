# ScriptBasic Windows 32 Bit

## Hints:

* To install the ScriptBasic web service use an admin console (**sbhttpd -install** | -remove) Use the Windows Service manager to **start** the service. Use **localhost:9090/home/echo** as your browser URL.

* To change the configuration file (SCRIBA.INI - compiled form) cd to the **bin** directory, edit SCRIBA.INI.txt then run **sbc -k SCRIBA.INI.txt**.

* The **example** directory scripts can be run from any location you prefer.

* IDE Hotkeys

```
ctrl-f - find/replace
ctrl-g - goto line
ctrl-z - undo
ctrl-y - redo

F2     - set breakpoint
F5     - go
F7     - single step
F9     - step out
F8     - step over
```

 
